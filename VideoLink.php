<?php 
namespace VideoParser;
/* Создание массива с данными из ссылки */
class VideoLink 
{	
	protected $link_string;
	public $parsed_url;
	protected $link_host_name;

	public function __construct($link_string = null){
		$this->link_string = $link_string;
		if($this->link_string) {
			$this->parsed_url = parse_url($this->link_string);
		}
		else {
			return false;
		}
	}
	
}

?>