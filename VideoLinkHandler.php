<?php
namespace VideoParser;
/* Обработка полученнего в VideoLink массива с содержимым ссылки*/

class VideoLinkHandler{
	
	protected $url_params;
	protected $parsed_url;
	public $parsed_url_array;
	protected $patterns_host = ['Youtube' => '/^(w{3}.)?youtu+(\.be)+$|(^(w{3}.)?youtube+\.com)+$/', 'Vimeo' => '/^(w{3}.)?(player.)?+vimeo+\.com+$/'];
	protected $patterns_iframe = [
			'Youtube' => '<iframe width="%2$s" height="%3$s" src="https://www.youtube.com/embed/%1$s" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
			'Vimeo' => '<iframe src="https://player.vimeo.com/video/%1$s" width="640" height="338" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>'
		];
	
	function __construct($parsed_url){
		$this->parsed_url = $parsed_url;
		$this->link_parsing();
	}
	
	//Разбор ссылки на имя и Id видео
	public function link_parsing(){
		$host = $this->parsed_url['host'];
		$path = $this->parsed_url['path'];
		$path_arr = array_filter(explode('/', $path));
		
		if(isset($this->parsed_url['query'])){
			parse_str($this->parsed_url['query'], $query_arr);
			$this->parsed_url_array['video_id'] = $query_arr['v'];
		}
		else {
			$this->parsed_url_array['video_id']  = end($path_arr);
		}

		foreach ($this->patterns_host as $key => $pattern) {
			if(preg_grep($pattern, [$host])){
				$this->parsed_url_array['host_name'] = $key;
				break;
			}
		}
		return $this->parsed_url_array;
	}
	
	//Генерация html кода iframe на основе паттерна $patterns_iframe 
	public function generate_iframe(int $width=640,int $height = 338){
		
		$video_id =	$this->parsed_url_array['video_id'];
		$host_name = $this->parsed_url_array['host_name'];
		
		return sprintf($this->patterns_iframe[$host_name],$video_id, $width, $height);
	}
}

?>