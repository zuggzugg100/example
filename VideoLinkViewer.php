<?php 
namespace VideoParser;
require('VideoLink.php');
require('VideoLinkHandler.php');
/* Вывод данных из ссылки */
class VideoLinkViewer{
	
	protected $parsed_link;
	protected $handler;
	
	function __construct($link_string){
		$videoLink = new VideoLink($link_string);
		$this->handler = new VideoLinkHandler($videoLink->parsed_url);
	}
	
	function get_host_name(){
		return $this->handler->parsed_url_array['host_name'];
	}
	
	function get_video_id(){
		return $this->handler->parsed_url_array['video_id'];
	}
	
	function get_iframe(){
		return $this->handler->generate_iframe();
	}
}

?>